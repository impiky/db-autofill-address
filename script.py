import os
import selenium
import configparser
import pyodbc
import time
import random
import pyautogui
import subprocess

import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from transliterate import get_translit_function

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = '1JROmkidlIUVouQA9WEsde987YeI_naw7PNptTBxduNI'
COLUMN_RANGE_NAME = 'A:E'

def get_config(section, key, configfile):
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = get_config("database", "Driver", "config.ini")
SERVER = get_config("database", "SERVER", "config.ini")
DATABASE = get_config("database", "DATABASE", "config.ini")
UID = get_config("database", "UID", "config.ini")
PWD = get_config("database", "PWD", "config.ini")


# Db manager class with operations methods
class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("查询失败")
            return False
        result = self.cur.fetchone()
        self.close()
        return result


def get_buyer_address(db_manager, buyer_id):
    # try:
    #     data = db_manager.fetchone("SELECT PostIndex, BuyerState, BuyerDistrict, BuyerCity, BuyerDeliveryAddress FROM Buyer WHERE BuyerID='{}'".format(buyer_id))
    #     if data[4] == None:
    #             print("No such record!") 
    #             return 'EMPTY'
    # except:
    #     return 'OK'
    return db_manager.fetchone("SELECT PostIndex, BuyerState, BuyerDistrict, BuyerCity, BuyerDeliveryAddress FROM Buyer WHERE BuyerID='{}'".format(buyer_id))
        

def get_number_of_rows(db_manager):
    return db_manager.fetchone("SELECT COUNT(*) FROM Buyer")


def get_data_and_write_to_db(db_manager, scopes, sreadsheet_id, column_range):
    creds = None
    buyer_id = 1
    translit_ru = get_translit_function("ru")
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_secret.json', scopes)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=sreadsheet_id,
                                range=column_range).execute()
    values = result.get('values', [])
    
    if not values:
        print('No data found.')
    else:
        while buyer_id != 1101:   
            for row in values:
                if row[0] == "Индекс":
                    continue
                    #  int(db_manager.fetchone("SELECT COUNT(1) FROM Buyer WHERE BuyerID='{}'".format(buyer_id))[0]) == 0
                elif not (db_manager.fetchone("SELECT BuyerDeliveryAddress FROM BUYER WHERE TRIM(BuyerDeliveryAddress) IS NOT NULL AND BuyerID='{}'".format(buyer_id)) is None):
                    
                    buyer_id += 1
                    continue 
                else:
                    if '\'' in translit_ru(row[4], reversed=True):
                        splitted_address = str(translit_ru(row[4], reversed=True)).split('\'')
                        row_tr = splitted_address[0] + splitted_address[1]
                        print(row[4])
                    else:
                        row_tr = translit_ru(row[4], reversed=True)
                    db_manager.execute("UPDATE Buyer SET PostIndex='{}', BuyerState='{}', BuyerDistrict='{}', BuyerCity='{}', BuyerDeliveryAddress='{}' WHERE BuyerID='{}'".format(row[0], translit_ru(row[1],
                        reversed=True), translit_ru(row[2], reversed=True), translit_ru(row[3], reversed=True), row_tr, buyer_id))
                    print("record with BuyerID:{} has been updated!".format(buyer_id))
                    buyer_id += 1
                     

db_manager = DbManager()
get_data_and_write_to_db(db_manager, SCOPES, SPREADSHEET_ID, COLUMN_RANGE_NAME)